﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace Elastic
{
    public class ElasticConnector
    {
        ElasticClient _ElasticClient;

        /// <summary>
        /// Connect to elasticsearch
        /// </summary>
        /// <param name="uri">http://localhost:9200</param>
        /// <param name="index">defulatIndex</param>
        /// <returns></returns>
        public bool Connect(string uri, string index)
        {
            try
            {
                //var node = new Uri("http://localhost:9200");

                //var settings = new ConnectionSettings(
                //    node,
                //    defaultIndex: "mazorsession_test"
                //);

                var settings = new ConnectionSettings(
                    new Uri(uri),
                    defaultIndex: index
                    );

                _ElasticClient = new ElasticClient(settings);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to connect elastic");
                throw e;
            }
        
        }
        
        /// <summary>
        /// Store and index an object. 
        /// Will be stored at /index/(object type)
        /// </summary>
        /// <param name="pocoObject">POCO object</param>
        /// <returns>Object id, or null if index failed.</returns>
        public string InsertObject(string pocoObject)
        {

            IIndexResponse index = _ElasticClient.Index(pocoObject);
           
            if (index.IsValid)
                return index.Id;

            return null;
        }
        
        public object Get(object o)
        {
            return null;
            
        }
    }
}
