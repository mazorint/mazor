﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace MazorSession
{
    public class MazorSession
    {
        public Guid SessionID { get; set; }
        public IPAddress ClientNetworkIP { get; set; }
        public int FirstSeqNum { get; set; }
        public IPAddress ServerNetworkIP { get; set; }
        public int ClientNetworkPort { get; set; }
        public int ServerNetworkPort { get; set; }
        public int NatPort { get; set; }
        public int OutKey { get; set; }
        public int InKey { get; set; }
        public Guid InKeyPacketId { get; set; }
        public Guid OutKeyPacketId { get; set; }
        public bool IsOver { get; set; }

    }
}
