﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redis
{
    public interface IRedisPublisher
    {
        bool Connect();
        bool Publish(string channelName, object o);
    }


    public interface IRedisSubscriber
    {
        bool Connect();
        bool Subscribe(string channelName);
        
        Action<T> OnMessageReceived<T>();
    }
}
